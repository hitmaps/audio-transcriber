import json
import whisper
import os
import shutil
import subprocess
import torch
import urllib.request
from pathlib import Path

# Load settings
settings = None
with open('config.json') as config:
    settings = json.load(config)

# Extract all audio files from RPKGs
processingDirectory = settings['processingDirectory']
stagingDirectory = os.path.join(processingDirectory, 'staging')
if not os.path.exists(stagingDirectory):
    os.makedirs(stagingDirectory)

print('Extracting WWES from RPKGs')
process = subprocess.Popen([f"{settings['rpkgCliLocation']}rpkg-cli.exe", '-extract_wwes_to_ogg_from', f"{settings['hitmanWoaLocation']}Runtime"],
                           cwd=stagingDirectory)
process.wait()


print('Combining RPKG folders')
sourceDir = os.path.join(processingDirectory, 'staging', 'WWES')
for item in os.listdir(sourceDir):
    rpkgDir = os.path.join(sourceDir, item)
    if os.path.isdir(rpkgDir):
        print(rpkgDir)
        shutil.copytree(rpkgDir, processingDirectory, dirs_exist_ok=True)

print('Retrieving current list of transcribed files from API')
headers = {
    'User-Agent': 'HITMAPS Transcriber'
}
request = urllib.request.Request('https://ioiapi.hitmaps.com/api/transcribed-audio/file-list', headers=headers)
response = urllib.request.urlopen(request)
transcriptions = json.loads(response.read())['data']
print('Finished fetching transcriptions')


model = whisper.load_model("medium")
print(torch.cuda.is_available())

skippedSubstrings = ["dth_poison", "dth_sltthrt", "gen_outbreakinfect", "gen_outbreaksick"]
index = 0
for path, subdirs, files in os.walk(f'{processingDirectory}\\assembly'):
    for name in files:
        if any(substring in name for substring in skippedSubstrings):
            print(f"{name} contains sounds that cannot be transcribed. Skipping.")
            continue

        fullName = os.path.join(path, name)
        transcriptionApiName = fullName.replace(f'{processingDirectory}\\', '').replace('\\', '/').replace('.ogg', '.wav')
        destinationName = fullName.replace("assembly", "transcribed\\assembly").replace(".ogg", ".txt")

        if os.path.isfile(destinationName):
            print(f"[SKIPPED] {name} is already transcribed on the filesystem. Skipping.")
            continue
        if transcriptionApiName in transcriptions:
            print(f'[SKIPPED] {name} is already transcribed on the server. Skipping.')
            continue

        
        transcription = model.transcribe(fullName)
        text = transcription["text"].strip()

        destinationPath = Path(destinationName)
        destinationPath.parent.mkdir(exist_ok=True, parents=True)
        destinationPath.write_text(text, encoding="utf8")

        print(f"[WRITTEN] {name}: text")

        index = index + 1
        if index % 10000 == 0:
            print(index)
