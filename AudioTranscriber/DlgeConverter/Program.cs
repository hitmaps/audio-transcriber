﻿// See https://aka.ms/new-console-template for more information

using System.Diagnostics;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using DlgeConverter;
using Microsoft.Data.SqlClient;

var secretReader = new SecretAppsettingReader();
#region DLGE Mass Export
var processInfo = new ProcessStartInfo
{
    WorkingDirectory = secretReader.ReadSection<string>("DlgeExtractDestination"),
    FileName = $"{secretReader.ReadSection<string>("RpkgCliLocation")}{Path.DirectorySeparatorChar}rpkg-cli.exe",
    Arguments = $"-extract_dlge_to_json_from \"{secretReader.ReadSection<string>("DlgeExtractSource")}\""
};
using var process = new Process();
process.StartInfo = processInfo;
//process.Start();
//process.WaitForExit();
#endregion

#region Transcription Upload to DB / B2

await using var connection = new SqlConnection(secretReader.ReadSection<string>("DbConnectionString"));
connection.Open();
var transcriptionWriter = new TranscriptionWriter(connection);
var existingTranscriptions = transcriptionWriter.GetTranscribedFiles();

var backblazeClient = new AmazonS3Client(new BasicAWSCredentials(
    secretReader.ReadSection<string>("Backblaze:KeyId"), 
    secretReader.ReadSection<string>("Backblaze:ApplicationKey")), new AmazonS3Config
{
    ServiceURL = "https://s3.us-west-002.backblazeb2.com",
});

var fileDirectory = Path.Join(secretReader.ReadSection<string>("DlgeExtractDestination"), "DLGE");
var index = 0;
var files = Directory.EnumerateFiles(fileDirectory, "*.dlge.json", SearchOption.AllDirectories).ToList();
foreach (var file in files)
{
    if (index++ % 1000 == 0)
    {
        Console.WriteLine($"{index:00000}/{files.Count}");
    }
    var json = JsonDocument.Parse(File.ReadAllText(file, Encoding.UTF8));
    var root = json.RootElement;
    var rootContainer = root.GetProperty("rootContainer");

    var transcriptions = TraverseJson(rootContainer, true);
    foreach (var transcription in transcriptions)
    {
        if (existingTranscriptions.Contains(transcription.DefaultWav))
        {
            Console.WriteLine($"[SKIPPED] {transcription.DefaultWav}");
            continue;
        }
        
        // Upload file to bucket
        await backblazeClient.PutObjectAsync(new PutObjectRequest
        {
            BucketName = secretReader.ReadSection<string>("Backblaze:BucketName"),
            Key = $"audio/{transcription.DefaultWav.Replace("assembly:", "assembly").Replace(".wav", ".ogg")}",
            FilePath = Path.Join(secretReader.ReadSection<string>("AudioLocation"), transcription.DefaultWav)
                .Replace("assembly:", "assembly")
                .Replace(".wav", ".ogg")
                .Replace("/", Path.DirectorySeparatorChar.ToString())
        });

        await transcriptionWriter.WriteTranscriptionAsync(transcription);
        Console.WriteLine($"[WRITTEN] {transcription.DefaultWav}");
    }
}

#endregion
new DirectoryInfo(Path.Join(secretReader.ReadSection<string>("DlgeExtractDestination"), "DLGE")).Delete(true);

return;

#region Helper Methods
List<Transcription> TraverseJson(JsonElement container, bool root)
{
    var transcriptions = new List<Transcription>();

    if (root)
    {
        // Copying this because I can't think of anything better atm
        var transcription = GetDefaultWavToTranscription(container);
        if (transcription != null)
        {
            transcriptions.Add(transcription);                
        }
        transcriptions.AddRange(TraverseJson(container, false));
    }
    else if (container.TryGetProperty("containers", out var childContainer))
    {
        foreach (var entry in childContainer.EnumerateArray())
        {
            var transcription = GetDefaultWavToTranscription(entry);
            if (transcription != null)
            {
                transcriptions.Add(transcription);                
            }
            transcriptions.AddRange(TraverseJson(entry, false));
        }
    }
    
    

    return transcriptions;
}

Transcription? GetDefaultWavToTranscription(JsonElement container)
{
    
    var hasDefaultWav = container.TryGetProperty("defaultWav", out var defaultWav);
    var hasLanguages = container.TryGetProperty("languages", out var languages);
    if (!hasDefaultWav || !hasLanguages || defaultWav.GetString() == null)
    {
        return null;
    }

    var result = new Transcription();
    // ReSharper disable once ForeachCanBeConvertedToQueryUsingAnotherGetEnumerator
    foreach (var language in languages.EnumerateObject())
    {
        var text = language.Value.GetString()!;
        text = Regex.Replace(text, @"//\[.+\]\\\\", string.Empty);
        text = Regex.Replace(text, @"^//\(\d+\.?\d*,\d+\.?\d*\)\\\\", string.Empty);
        text = Regex.Replace(text, @"//\(\d+\.?\d*,\d+\.?\d*\)\\\\$", string.Empty);
        text = Regex.Replace(text, @"//\(\d+\.?\d*,\d+\.?\d*\)\\\\", " ");

        var fileName = defaultWav.GetString()!;
        var match = Regex.Match(fileName, @"^\[(?<FileName>.+)\].+$");
        fileName = match.Groups["FileName"].Value;

        if (language.Name == "en")
        {
            result.DefaultWav = fileName;
            result.Text = text;
        }
        else
        {
            result.Translations.Add(new Translation
            {
                LanguageCode = language.Name,
                Text = text
            });            
        }
    }

    return !string.IsNullOrWhiteSpace(result.DefaultWav) ? result : null;
}
#endregion