﻿namespace DlgeConverter.Models;

public class Transcriptions
{
    public string FullPath { get; set; } = null!;
    public string FileName { get; set; } = null!;
    public string Transcription { get; set; } = null!;
    public string Status { get; set; } = null!;
    public string Source { get; set; } = null!;
}