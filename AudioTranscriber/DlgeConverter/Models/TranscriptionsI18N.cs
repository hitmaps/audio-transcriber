﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DlgeConverter.Models;

[Table("TranscriptionsI18N")]
public class TranscriptionsI18N
{
    [Key]
    public int Id { get; set; }
    public int TranscriptionsId { get; set; }
    public string LanguageCode { get; set; } = null!;
    public string Transcription { get; set; } = null!;
    public string Status { get; set; } = null!;
    public string Source { get; set; } = null!;
}