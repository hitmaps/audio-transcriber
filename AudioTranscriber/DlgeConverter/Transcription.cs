﻿namespace DlgeConverter;

public class Transcription
{
    public string DefaultWav { get; set; } = null!;
    public string Text { get; set; } = null!;
    public List<Translation> Translations { get; set; } = [];
}

public class Translation
{
    public string LanguageCode { get; set; } = null!;
    public string Text { get; set; } = null!;
}