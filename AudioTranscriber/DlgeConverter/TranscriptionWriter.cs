﻿using Dapper;
using Dapper.Bulk;
using DlgeConverter.Models;
using Microsoft.Data.SqlClient;

namespace DlgeConverter;

public class TranscriptionWriter
{
    private readonly SqlConnection connection;

    private const string SelectAllSql = "SELECT * FROM Transcriptions";
    private const string InsertSql =
        "INSERT INTO Transcriptions (FullPath, FileName, Transcription, Status, Source) VALUES (@FullPath, @FileName, @Transcription, @Status, @Source)";

    private const string Source = "Game Files (DLGE)";
    private const string Status = "Verified";

    public TranscriptionWriter(SqlConnection connection)
    {
        this.connection = connection;
    }

    public List<string> GetTranscribedFiles()
    {
        var transcriptions = connection.Query<Transcriptions>(SelectAllSql);

        return transcriptions.Select(x => x.FullPath).ToList();
    }

    public async Task WriteTranscriptionAsync(Transcription transcription)
    {
        // Insert base record
        var dbModel = new Transcriptions
        {
            FullPath = transcription.DefaultWav,
            Transcription = transcription.Text,
            FileName = Path.GetFileName(transcription.DefaultWav),
            Source = Source,
            Status = Status
        };

        int transcriptionId;
        try
        {
            transcriptionId = await connection.ExecuteAsync(InsertSql, dbModel);
        }
        catch (SqlException e)
        {
            Console.WriteLine(e.ToString());
            return;
        }
        
        // Insert I18N records
        var dbTranslations = transcription.Translations.Select(translation => new TranscriptionsI18N
        {
            TranscriptionsId = transcriptionId,
            LanguageCode = translation.LanguageCode,
            Transcription = translation.Text,
            Status = Status,
            Source = Source
        }).ToList();
        await connection.BulkInsertAsync(dbTranslations);
    }
}