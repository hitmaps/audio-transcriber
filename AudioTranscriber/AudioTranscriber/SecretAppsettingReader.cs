﻿using Microsoft.Extensions.Configuration;

namespace AudioTranscriber;

public class SecretAppsettingReader
{
    public T? ReadSection<T>(string sectionName)
    {
        var builder = new ConfigurationBuilder()
            .AddUserSecrets<Program>()
            .AddEnvironmentVariables();
        var configurationRoot = builder.Build();

        return configurationRoot.GetSection(sectionName).Get<T>();
    }
}