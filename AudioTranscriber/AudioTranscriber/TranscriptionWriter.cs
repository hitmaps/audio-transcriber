﻿using AudioTranscriber.Models;
using Dapper;
using Microsoft.Data.SqlClient;

namespace AudioTranscriber;

public class TranscriptionWriter
{
    private readonly SqlConnection connection;

    private const string SelectAllTranscriptionsSql = "SELECT FullPath FROM Transcriptions";
    private const string SelectSkippedFilesSql = "SELECT FullPath FROM SkippedFile";

    private const string SelectAlreadyHandledSql =
        "SELECT 1 FROM Transcriptions WHERE FullPath = @FullPath UNION " +
        "SELECT 1 FROM SkippedFile WHERE FullPath = @FullPath";
    
    private const string InsertSql =
        "INSERT INTO Transcriptions (FullPath, FileName, Transcription, Status, Source) VALUES (@FullPath, @FileName, @Transcription, @Status, @Source)";

    private const string InsertSkippedFileSql = "INSERT INTO SkippedFile (FullPath) VALUES (@FullPath)";

    public TranscriptionWriter(SqlConnection connection)
    {
        this.connection = connection;
    }

    public bool TranscriptionExists(string fullPath)
    {
        return connection.Query(SelectAlreadyHandledSql, new { FullPath = fullPath }).Any();
    }

    public async Task<List<string>> GetTranscribedFilesAsync()
    {
        throw new NotImplementedException("Retired method");
        var transcriptions = connection.Query(SelectAllTranscriptionsSql);
        var skippedFiles = await connection.QueryAsync(SelectSkippedFilesSql);

        var results = new List<string>();
        foreach (var transcription in transcriptions)
        {
            results.Add(transcription.FullPath);
        }

        foreach (var skippedFile in skippedFiles)
        {
            results.Add(skippedFile.FullPath);
        }
        
        return results;
    }

    private class TranscribedAndSkippedFiles
    {
        public string FullPath { get; set; } = null!;
    }

    public async Task WriteTranscriptionAsync(string filePath, string ioiPath, string transcription)
    {
        var transcribedText = new Transcriptions
        {
            FileName = Path.GetFileName(filePath).Replace(".txt", ".wav"),
            FullPath = ioiPath,
            Transcription = transcription.Trim(),
            Source = "OpenAI",
            Status = "Transcribed"
        };
        
        await connection.ExecuteAsync(InsertSql, transcribedText);
    }

    public async Task AddSkippedFileAsync(string ioiPath)
    {
        await connection.ExecuteAsync(InsertSkippedFileSql, new SkippedFile
        {
            FullPath = ioiPath
        });
    }
}