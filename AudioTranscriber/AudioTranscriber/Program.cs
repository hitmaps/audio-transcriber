﻿// See https://aka.ms/new-console-template for more information

using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using AudioTranscriber;
using Microsoft.Data.SqlClient;

var secretReader = new SecretAppsettingReader();

await using var connection = new SqlConnection(secretReader.ReadSection<string>("DbConnectionString"));
var transcriptionWriter = new TranscriptionWriter(connection);

var backblazeClient = new AmazonS3Client(new BasicAWSCredentials(
    secretReader.ReadSection<string>("Backblaze:KeyId"), 
    secretReader.ReadSection<string>("Backblaze:ApplicationKey")), new AmazonS3Config
{
    ServiceURL = "https://s3.us-west-002.backblazeb2.com",
});

var rootDirectory = Path.Join(secretReader.ReadSection<string>("AudioLocation"), "transcribed", "assembly");

var index = 0;
foreach (var file in Directory.EnumerateFiles(rootDirectory, "*.txt", SearchOption.AllDirectories))
{
    index++;
    var fullIoiPath = file.Replace(rootDirectory, "assembly:")
        .Replace(Path.DirectorySeparatorChar, '/')
        .Replace(".txt", ".wav");
    if (transcriptionWriter.TranscriptionExists(fullIoiPath))
    {
        Console.WriteLine($"{file} already transcribed. Skipping.");
        continue;
    }

    var transcription = File.ReadAllText(file);
    if (!string.IsNullOrWhiteSpace(transcription))
    {
        Console.WriteLine($"{fullIoiPath}: {transcription}");
        
        // Upload file to bucket
        await backblazeClient.PutObjectAsync(new PutObjectRequest
        {
            BucketName = secretReader.ReadSection<string>("Backblaze:BucketName"),
            Key = $"audio/{fullIoiPath.Replace("assembly:", "assembly").Replace(".wav", ".ogg")}",
            FilePath = file.Replace($"transcribed{Path.DirectorySeparatorChar}", "").Replace(".txt", ".ogg")
        });
        
        await transcriptionWriter.WriteTranscriptionAsync(file, fullIoiPath, transcription);
    }
    else
    {
        Console.WriteLine($"No transcription: {transcription}");
        await transcriptionWriter.AddSkippedFileAsync(fullIoiPath);
    }
    
    if (index % 1000 == 0)
    {
        Console.WriteLine($"Processed {index} files");
    }
}