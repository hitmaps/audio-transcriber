﻿namespace AudioTranscriber.Models;

public class SkippedFile
{
    public string FullPath { get; set; } = null!;
}